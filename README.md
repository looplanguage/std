# Standard Library

In this directory you will find a standard library for Loop. It is implemented in Rust and Loop itself.

> **Note:** This is in heavy development and thus not finished

## Compilation guide

1. Make sure you have installed Cargo (version > 1.60)
2. `cargo build --release`

## Documentation

Clone the repository and in the terminal execute this command: `cargo doc --open`, this will open generated documentation

## Content

The STD is divided into categories, the functions are listed below listed by category:

### console

- `void println(const char* ptr);`
- `void print(const char* ptr);`
- `const char* input(const char* ptr);`

### system

- `const char* execute(const char* str_ptr);`
- `void exit(int code);`

### fs

- `const char* read_file(const char* file_location);`
- `bool write_file(const char* file_location, const char* content);`
- `const char* cwd();`
- `const char* ced();`

### time

- `int now();`

### string

- `const char* trim_start(const char* str, const char* pattern);`
- `const char* trim_end(const char* str, const char* pattern);`
- `const char* insert(const char* str, const char* insert, int idx);`
- `const char* replace(const char* str, int begin, int end, const char* replacement);`

## Example

This is an example of how to use the library directly without the abstraction written in Loop around it:

```
import "std" as std

file_loc := "file.txt"

std.println("Hello, this is std")
_ := std.write_file(file_loc, "I have written a file")
content := std.read_file(file_loc)
std.println(content)
```
