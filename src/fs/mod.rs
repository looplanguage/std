use std::ffi::CString;
use std::io::Write;
use std::os::raw::c_char;

use crate::ptr_to_string;

/// Expects a relative or absolute file location and returns content of the file as a char pointer.
/// If the file does not exist, than it will return a char pointer with an error message
#[no_mangle]
pub extern "C" fn read_file(file_location: *const c_char) -> *const c_char {
    let res = std::fs::read_to_string(ptr_to_string(file_location))
        .expect("An error occured while reading the file");
    CString::new(res)
        .expect("Could not parse content of file")
        .into_raw()
}

/// Expects a file location and a char pointer as file content.
/// Returns a bool if success `true` is error than `false`
#[no_mangle]
pub extern "C" fn write_file(file_location: *const c_char, content: *const c_char) -> bool {
    let file = std::fs::File::create(ptr_to_string(file_location));
    if let Err(_) = file {
        return false;
    }
    match file {
        Err(_) => return false,
        Ok(mut ok) => {
            let res = ok.write_all(ptr_to_string(content).as_bytes());
            if let Err(_) = res {
                return false;
            }
        }
    }

    true
}

/// Returns in a char pointer the current working directory of the user
#[no_mangle]
pub extern "C" fn cwd() -> *const c_char {
    let path = std::env::current_dir();

    let path = if let Ok(suc) = path {
        suc.display().to_string()
    } else {
        String::from("ERROR: Could get current working directory")
    };

    CString::new(path)
        .expect("ERROR: Could not convert path to CString")
        .into_raw()
}

/// Returns in a char pointer the current executable directory of the user
#[no_mangle]
pub extern "C" fn ced() -> *const c_char {
    let path = std::env::current_exe();

    let path = if let Ok(suc) = path {
        suc.display().to_string()
    } else {
        String::from("ERROR: Could get current executable directory")
    };

    CString::new(path)
        .expect("ERROR: Could not convert path to CString")
        .into_raw()
}
