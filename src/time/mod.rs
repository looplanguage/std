use std::time::{SystemTime, UNIX_EPOCH};

/// Returns and integer of the UNIX_EPOCH time in ms.
/// Subtracted 1655700000000, otherwise integer overflow
/// Only use for things like timers
#[no_mangle]
pub extern "C" fn now() -> i32 {
    let start = SystemTime::now();
    let since_the_epoch = start
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards POG");
    (since_the_epoch.as_millis() - 1655700000000) as i32
}
