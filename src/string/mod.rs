use crate::{ptr_to_string, string_to_ptr};
use std::os::raw::c_char;

/// Trims the start of that provided string when the provided char matches the start
#[no_mangle]
pub extern "C" fn trim_start(str: *const c_char, pattern: *const c_char) -> *const c_char {
    let str = ptr_to_string(str);
    let _match = ptr_to_string(pattern);

    string_to_ptr(str.trim_start_matches(_match.as_str()).to_string())
}

/// Trims the end of that provided string when the provided char matches the end
#[no_mangle]
pub extern "C" fn trim_end(str: *const c_char, pattern: *const c_char) -> *const c_char {
    let str = ptr_to_string(str);
    let _match = ptr_to_string(pattern);

    string_to_ptr(str.trim_end_matches(_match.as_str()).to_string())
}

/// Inserts a string into another string at given index
#[no_mangle]
pub extern "C" fn insert(str: *const c_char, insert: *const c_char, idx: i32) -> *const c_char {
    let mut str = ptr_to_string(str);
    let insert = ptr_to_string(insert);

    str.insert_str(idx as usize, &*insert);
    string_to_ptr(str)
}

/// Replaces a substring at the given begin and end index
#[no_mangle]
pub extern "C" fn replace(str: *const c_char, begin: i32, end: i32, replacement: *const c_char) -> *const c_char {
    let str = ptr_to_string(str);
    let replacement = ptr_to_string(replacement);

    let left = str[..begin as usize].to_string();
    let right = str[end as usize..].to_string();

    let str = format!("{}{}{}", left, replacement, right);

    string_to_ptr(str)
}