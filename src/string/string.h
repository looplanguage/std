const char* trim_start(const char* str, const char* pattern);
const char* trim_end(const char* str, const char* pattern);
const char* insert(const char* str, const char* insert, int idx);
const char* replace(const char* str, int begin, int end, const char* replacement);
