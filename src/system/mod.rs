use crate::ptr_to_string;
use std::ffi::CString;
use std::os::raw::c_char;
use std::process::Command;

/// Prints command of type char pointer in terminal and returns stdout as char pointer
/// Works on Unix and Windows
#[no_mangle]
pub extern "C" fn execute(str_ptr: *const c_char) -> *const c_char {
    let input = ptr_to_string(str_ptr);
    let split: Vec<&str> = input.split(' ').collect();
    let split: Vec<String> = split.iter().map(|x| x.to_string()).collect();

    let output = {
        #[cfg(unix)]
        {
            let mut args = split.clone();
            args.remove(0);
            Command::new(split.first().unwrap())
                .args(args)
                .stdout(std::process::Stdio::piped())
                .spawn()
                .expect("failed to execute process")
        }

        #[cfg(target_os = "windows")]
        {
               Command::new("cmd")
                .arg("/C")
                .args(split)
                .stdout(std::process::Stdio::piped())
                .spawn()
                .expect("failed to execute process")
        }
    };

    let output = output.wait_with_output()
    .expect("failed to wait on child");

    let str = String::from_utf8(output.stdout).unwrap();

    CString::new(str)
        .expect("Could not parse content of file")
        .into_raw()
}

/// Exits the program with given code
#[no_mangle]
pub extern "C" fn exit(code: i32) {
    std::process::exit(code);
}
