// ===========================================================
// BEGINNING OF RE-EXPORTS OF FUNCTIONS CREATED IN SUBFOLDER
// ===========================================================

/// Functions for terminal stuff, like printing
mod console;
pub use console::*;

/// Functions for using the file system
mod fs;
pub use fs::*;

/// Functions for doing systems things: executing commands, exiting programs, etc
mod system;
pub use system::*;

/// Functions regarding strings
mod time;
pub use time::*;

/// Functions regarding time
mod string;
pub use string::*;

// ===========================================================
// END OF RE-EXPORTS OF FUNCTIONS CREATED IN SUBFOLDER
// ===========================================================

use std::ffi::{CStr, CString};
use std::os::raw::c_char;
/// Function to convert a char pointer to a String.
/// The FFI does not have a String so you will always get a char pointer
/// Use this function to convert it to a String
pub fn ptr_to_string(str: *const c_char) -> String {
    let c_buf: *const c_char = str;
    let c_str: &CStr = unsafe { CStr::from_ptr(c_buf) };
    let str_slice: &str = c_str.to_str().unwrap();
    str_slice.to_owned()
}

/// Function to convert a String to a char pointer.
/// FFI does not support String so when you return you have to convert to char pointer
/// Use this function
pub fn string_to_ptr(str: String) -> *const c_char {
    CString::new(str)
        .expect("Could not parse String to char pointer")
        .into_raw()
}

/// *NOT NOT RENAME THIS FUNCTION*
/// This is called by Loop for all the function signatures
#[no_mangle]
pub extern "C" fn library_signatures() -> *const c_char {
    // Declaring the variable to put all function signatures in
    let mut function_signatures = String::new();

    // At compile time the header files THAT SHOULD BE AT THE ROOT OF THE FOLDER will be loaded
    // and pushed to the string.
    // If you added a new category of STD functions you need to a new header file...
    function_signatures.push_str(include_str!("system/system.h"));
    function_signatures.push_str(include_str!("fs/fs.h"));
    function_signatures.push_str(include_str!("console/console.h"));
    function_signatures.push_str(include_str!("time/time.h"));
    function_signatures.push_str(include_str!("string/string.h"));

    // Converting it to a CString and then into a char pointer
    CString::new(function_signatures)
        .expect("CString::new failed")
        .into_raw()
}
