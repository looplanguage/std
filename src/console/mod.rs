use std::ffi::CString;
use std::io::BufRead;
use std::os::raw::c_char;

use crate::ptr_to_string;

/// Prints the given string of type char pointer in the terminal with a newline
#[no_mangle]
pub extern "C" fn println(ptr: *const c_char) {
    let str = ptr_to_string(ptr);
    println!("{}", str);
}

/// Prints the given string of type char pointer in the terminal
#[no_mangle]
pub extern "C" fn print(ptr: *const c_char) {
    let str = ptr_to_string(ptr);
    print!("{}", str);
}

/// Prints an input in the STDIN and returns the response hte
#[no_mangle]
pub extern "C" fn input(ptr: *const c_char) -> *const c_char {
    // print(ptr), has a really weird but where it
    // does not print your message before reading the std, but after...
    println(ptr);

    let mut line = String::new();
    std::io::stdin().lock().read_line(&mut line).unwrap();
    CString::new(line)
        .expect("Could not parse input line")
        .into_raw()
}
